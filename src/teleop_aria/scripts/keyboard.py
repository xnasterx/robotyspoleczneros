#!/usr/bin/env python
import rospy
import curses
from geometry_msgs.msg import Twist


def keyboard():
    pub = rospy.Publisher('/RosAria/cmd_vel', Twist, queue_size=10)
    rospy.init_node('teleop_aria_keyboard', anonymous=True)
    r = rospy.Rate(10) # 10hz
    stdscr = curses.initscr()
    curses.cbreak()
    stdscr.keypad(1)
    stdscr.refresh()
    stdscr.nodelay(1)
    curses.beep()
    lastmsg = None
    while not rospy.is_shutdown():
        key = stdscr.getch()
        t = Twist()
        t.linear.y = 0.0
        t.angular.x, t.angular.y, t.angular.z = (0.0, 0.0, 0.0)
        stdscr.refresh()
        if key == curses.KEY_UP:
            t.linear.x = 1.0
            t.angular.z = 0.0
            lastmsg = None
            pub.publish(t)
        elif key == curses.KEY_DOWN:
            t.linear.x = -1.0
            t.angular.z = 0.0
            lastmsg = None
            pub.publish(t)
        elif key == curses.KEY_LEFT:
            t.linear.x = 0.0
            t.angular.z = 1.0
            lastmsg = None
            pub.publish(t)
        elif key == curses.KEY_RIGHT:
            t.linear.x = 0.0
            t.angular.z = -1.0
            lastmsg = None
            pub.publish(t)
        elif lastmsg != "STOP":
            lastmsg = "STOP"
            t.linear.x = 0.0
            t.angular.z = 0.0
            pub.publish(t)
        r.sleep()
    curses.endwin()

if __name__ == '__main__':
    try:
        keyboard()
    except rospy.ROSInterruptException:
        pass
